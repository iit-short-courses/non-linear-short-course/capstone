students = ['belle', 'ariel', 'aurora', 'anna', 'tiana', 'merida']
grades = [60, 80, 95, 85, 89, 97]

def newStudent(students, grades):
    newStudentName = str(input("New Student Name: "))
    newStudentGrade = int(input("New Student Grade: "))
    if(newStudentGrade >= 60 and newStudentGrade <= 100):
        students = students.append(newStudentName)
        grades = grades.append(newStudentGrade)
    elif(newStudentGrade > 100):
        print(f"The student grade cannot be greater than 100")
    elif(newStudentGrade < 60):
        print(f"The student grade cannot be less than 60")
    return

def printAllStudents(students):
    print(f"All Students:  {students}")
    return

def printAllGrades(grades):
    print(f"All grades:  {grades}") 
    return

def individualStudentGrade(students, grades):
    studentName = str(input("Look up which student? "))
    for i in range(len(students)):
        if(studentName == students[i]):
            print(f"Student {studentName} has grade {grades[i]}")
            return
    print(f"Student does not exist")
    return

def studentWithHighestGrade(students, grades):
    indexWithMaxGrade=0
    highestGrade = max(grades)
    for i in range(len(grades)):
        if(highestGrade == grades[i]):
            indexWithMaxGrade = i
            break
    print(f"The student with the highest grade is {students[indexWithMaxGrade]}")
    print(f"His or her grade is {grades[indexWithMaxGrade]}")
    return

def studentWithLowestGrade(students, grades):
    indexWithMinGrade=0
    highestGrade = min(grades)
    for i in range(len(grades)):
        if(highestGrade == grades[i]):
            indexWithMinGrade = i
            break
    print(f"The student with the lowest grade is {students[indexWithMinGrade]}")
    print(f"His or her grade is {grades[indexWithMinGrade]}")
    return

def generateGraph(students, grades):
    import matplotlib.pyplot as plt
    import numpy as np
    grade_labels = ["60-70 (lowest)", "71-80", "81-90", "91-95", "96-100 (highest)"]
    y = np.array([0, 0, 0, 0, 0])
    grade_explode = [0, 0, 0, 0, 0.1]
    # for 0-59
    # for 60-70
    # for 71-80
    # for 81-90
    # for 91-95
    # for 96-100

    for i in range(len(grades)):
        if(grades[i] >=0 and grades[i] < 60 ):
            print("The grade cannot be less than 60.")
        elif (grades[i] >= 60 and grades[i] <=70 ):
            y[0] = y[0] + 1
        elif (grades[i] >= 71 and grades[i] <=80 ):
            y[1] = y[1] + 1
        elif (grades[i] >= 81 and grades[i] <=90 ):
            y[2] = y[2] + 1
        elif (grades[i] >= 91 and grades[i] <=95 ):
            y[3] = y[3] + 1
        else:
            y[4] = y[4] + 1
    
    plt.title('Grades of all students')
    plt.pie(y, labels = grade_labels, autopct='%1.0f%%', startangle = 0, explode = grade_explode)
    plt.show()
    return


def continue_to_menu():
    print(input('\nPress ENTER to continue'))
    main_menu()

def main_menu():
    global students, grades

    menu = """
    [program begins]
    Menu:
    1. Add a new student & grade
    2. Print all students
    3. Print all grades
    4. Lookup a student's grade
    5. Find student with highest grade
    6. Find student with lowest grade
    7. Generate graph
    8. Quit
    """
    print(menu)

    option = int(input("Choose an option: "))
    if(option == 1):
        newStudent(students, grades)
        continue_to_menu()
    elif(option == 2):
        printAllStudents(students)
        continue_to_menu()
    elif(option == 3):
        printAllGrades(grades)
        continue_to_menu()
    elif(option == 4):
        individualStudentGrade(students, grades)
        continue_to_menu()
    elif(option == 5):
        studentWithHighestGrade(students, grades)
        continue_to_menu()
    elif(option == 6):
        studentWithLowestGrade(students, grades)
        continue_to_menu()
    elif(option == 7):
        generateGraph(students, grades)
        continue_to_menu()
    elif(option == 8):
        return
    else:
        print(f"The options must be an integer from 1 to 8")
        continue_to_menu()
            
main_menu()

